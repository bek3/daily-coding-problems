cmake_minimum_required(VERSION 3.10)
project(daily_coding_problems)

set(CMAKE_CXX_STANDARD 17)

include_directories(resources)

set(RESOURCE_SRC resources/vector_tools.cpp)

add_executable(1.1-product-of-arr-elements.out problems/1.1-get-product-of-elements/1-1.c)
add_executable(1.2-locate-sorting-window.out problems/1.2-locate-sorting-window/1-2.cpp)
add_executable(1.3-largest-subarray-sum.out problems/1.3-largest-subarray-sum/1-3.c)
add_executable(1.4-smaller-elements-to-right.out problems/1.4-smaller-elements-to-right/1-4.cpp resources/vector_tools.cpp)
add_executable(2.1-find-anagram-indeces problems/2.1-find-anagram-indeces/2-1.cpp resources/vector_tools.cpp)
add_executable(2.2-generate-palindrome-pairs problems/2.2-generate-palindrome-pairs/2-2.cpp)
