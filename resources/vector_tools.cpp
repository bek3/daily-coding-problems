//
// Created by b on 5/14/21.
//

#include "vector_tools.h"

void vectortools::print_vector(std::vector<int> vec){
    for (const auto & item : vec){
        std::cout << item << " ";
    }
    std::cout << std::endl;
}
