//
// Created by b on 5/14/21.
//

#include <vector>
#include <iostream>

#ifndef DAILY_CODING_PROBLEMS_VECTOR_TOOLS_H
#define DAILY_CODING_PROBLEMS_VECTOR_TOOLS_H

namespace vectortools{
    void print_vector(std::vector<int>);
}

#endif //DAILY_CODING_PROBLEMS_VECTOR_TOOLS_H
