# Daily Coding Problems

Taken from Alex Miller and Lawrence Wu's book available at
https://www.amazon.com/gp/product/1793296634/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1

## List of Problems
| Problem		| Language	| Date		| Description						|
|:----------------------|:--------------|:--------------|:------------------------------------------------------|
| 1.1			| C		| 21 Mar. 2019	| Calculate product of all other elements in array.	|
| 1.2			| Python	| 25 Mar. 2010	| Locate smallest window of elements to sort in array.	|
| 1.3			| C	|		| Compute largest possible subarray sum.		|
