//
// Created by b on 5/14/21.
//

#include <iostream>
#include <string>
#include <vector>

#include "vector_tools.h"

const std::string base = "Hello World";
const std::string smaller1 = "l";
const std::string smaller2 = "Wo";

std::vector<int> count_anagrams(std::string sub){
    std::vector<int> res;

    // TODO: Actually mix up string
    for (int i=0; i<base.size(); i++){
        if (base[i] == sub[0]){
            res.push_back(i);
        }
    }
    return res;
}

int main(){

    std::cout << "Base string: " << base << std::endl;

    std::vector<int> res1 = count_anagrams(smaller1);
    std::vector<int> res2 = count_anagrams(smaller2);

    std::cout << "Substring " << smaller1 << " Result: ";
    vectortools::print_vector(res1);

    std::cout << "Substring " << smaller2 << " Result: ";
    vectortools::print_vector(res2);

    return 0;
}
