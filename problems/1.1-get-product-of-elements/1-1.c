/*
 * Problem 1.1
 * Brendan Kristiansen
 * From "Daily Coding Problem" by Alex Miller and Lawrence Wu
 *
 * Arrays - Get Products of All Other Elements
 * 
 * Given an array of integers, return a new array that contains the product
 * of all numbers in the original array except the one located at i.
 */

#include <stdio.h>
#include <stdlib.h>

int calc_product(int*, int, int);
void generate_products(int*, int*, int);
void print_arr(int*, int);

int main(void){
	int arr_len = 10;
	int low = 1;
	int hi = 15;
	int i;
	int* arr;
	int* prod_arr;

	srand(7);
	arr = (int*) malloc(sizeof(int) * arr_len);
	prod_arr = (int*) malloc(sizeof(int) * arr_len);
	for (i=0; i<arr_len; i++){
		arr[i] = rand() % (hi + 1 - low) + low;
	}
	
	print_arr(arr, arr_len);
	
	generate_products(arr, prod_arr, arr_len);

	print_arr(prod_arr, arr_len);
	return 0;
}

// Iterate through array and populate new array with products
void generate_products(int* arr, int* products, int len){
	int i;

	for (i=0; i<len; i++){
		products[i] = calc_product(arr, len, i);
	}
}

// Calculate the value of a given index of the new array
int calc_product(int* arr, int len, int i){
	int j;
	int prod = 1;

	for (j=0; j<len; j++){
		if (j == i){
			continue;
		}else{
			prod *= arr[j];
		}
	}

	return prod;
}

// Print array
void print_arr(int* arr, int len){
	int i;
	for (i=0; i<len; i++){
		printf("%d\n", arr[i]);
	}
	printf("END OF ARRAY\n");
}
