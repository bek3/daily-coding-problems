//
// Created by b on 5/14/21.
//

#include<iostream>
#include<vector>

#include "vector_tools.h"

const std::vector<int> arr = {6, 5, 4, 3, 2, 1};

std::vector<int> count_smaller(std::vector<int> arr){
    std::vector<int> smaller;

    for (int i=arr.size()-1; i >= 0; i--){
         int count = 0;
         for (int j=i; j<arr.size(); j++){
             if (arr[j]<arr[i]) count++;
         }
         smaller.push_back(count);
    }
    // TODO: Reverse vector
    return smaller;
}

int main(void){

    std::vector<int> smaller = count_smaller(arr);

    vectortools::print_vector(arr);
    vectortools::print_vector(smaller);

    return 0;
}
