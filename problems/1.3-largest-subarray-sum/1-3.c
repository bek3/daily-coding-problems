#include <stdio.h>

const int ARR[] = {1, 3, 4, 5, -2, -4, -8, 12, 23, 55};
const int ARR_LEN = 10;

int main(void){
    int i;
	int highest_sum = 0;
	int current_sum = 0;

    printf("Array input: ");
	for (i=0; i<ARR_LEN; ++i){
		printf("%d", ARR[i]);
		if (i < ARR_LEN-1){
		    printf(", ");
		}
	}
	printf("\n");

	for (i=0; i<ARR_LEN; i++){
		if (ARR[i] >=0){
		    current_sum += ARR[i];
		}else{
		    if (current_sum > highest_sum){
				highest_sum = current_sum;
			}
			current_sum = 0;
		}
	}

	highest_sum = (current_sum > highest_sum) ? current_sum : highest_sum;

	printf("Largest subarray sum: %d\n", highest_sum);

    return 0;
}
