//
// Created by b on 5/14/21.
//

#include <iostream>
#include <vector>

const std::vector<int> sorting1 = {1,2,3,7,6,5,8,9,10};
const std::vector<int> sorting2 = {2,5,7,9,8,3,10,15};
const std::vector<int> sorting3 = {3,7,5,6,9};
const std::vector<std::vector<int>> all_arrs = {sorting1, sorting2, sorting3};

class SortingBounds {
public:
    int lo;
    int hi;
};

SortingBounds get_bounds(std::vector<int> arr){
    SortingBounds bounds{};
    bounds.lo = 0;
    bounds.hi = arr.size()-1;

    for (int i=0; i<arr.size(); i++){
        if (arr[i] > arr[i+1]){
            bounds.lo = i;
            break;
        }
    }

    for (int i=0; i<arr.size(); i++){
        int j = arr.size()-i-1;
        if (j<0){
            continue;
        }
        if (arr[j] < arr[j-1]){
            bounds.hi = i+1;
            break;
        }
    }

    return bounds;
}

int main(){

    for (const auto & all_arr : all_arrs){
        auto bounds = get_bounds(all_arr);
        std::cout << "Array: ";
        for (const auto &arr : all_arr){
            std::cout << arr << ", ";
        }
        std::cout << std::endl;

        std::cout << "Bounds: " << bounds.lo << ", " << bounds.hi << std::endl;
    }

    return 0;
}
