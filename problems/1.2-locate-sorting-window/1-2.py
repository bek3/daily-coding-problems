#!/usr/bin/env python3

# Problem 1.1
# Brendan Kristiansen
# From "Daily Coding Problem" by Alex Miller and Lawrence Wu
#
# Arrays - Locate smallest window needing to be sorted
#
# Given an adday of integers, determine the boundaries of the smallest window that must
# be sorted for the entire array to be put in order.

arr1 = [1,2,3,7,6,5,8,9,10]
arr2 = [2,5,7,9,8,3,10,15]
arr3 = [3,7,5,6,9]

arrays = [arr1,arr2,arr3]

def get_bounds(arr):
    low = 0
    hi = 0

    for i in range(len(arr)):
        if arr[i] > arr[i+1]:
            low = i
            break

    for j in range(len(arr)):
        i = len(arr) - j - 1
        if i < 0:
            continue
        if arr[i] < arr[i-1]:
            hi = i
            break

    return (low,hi+1)

def main():
    for arr in arrays:
        (low,hi) = get_bounds(arr)
        print("Results for "+str(arr)+": "+str(low)+", "+str(hi))

if __name__ == "__main__":
    main()
